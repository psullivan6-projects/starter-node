const argv = require('yargs').argv;
require('dotenv').config();

/**
 * Return the current node environment, letting the dev override the APP_ENV .env variable by
 * passing an --env=ENVIRONMENT_HERE flag
 *
 * @param  {Boolean} readDotEnv   Condition on whether or not to read/parse the .env file
 * @return {String}               THE environment
 */
module.exports = (readDotEnv = true) => {
  if (argv.env != null) {
    return argv.env;
  }

  if ((process.env.ENVIRONMENT != null) && (readDotEnv)) {
    return process.env.ENVIRONMENT;
  }

  return 'local';
};
