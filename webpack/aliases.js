// Variables

const aliases = directory => ({
  Components : `${directory}/src/components/`,
  Utilities  : `${directory}/src/utilities/`,
  Styles     : `${directory}/src/styles/`,
  NPM        : `${directory}/node_modules/`,
  Main       : `${directory}/src/js/`,
});

module.exports = aliases;
