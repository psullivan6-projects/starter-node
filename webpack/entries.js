const glob = require('glob');
const path = require('path');
// Utilities
//
// just return an empty-string as default ... can add prefixes as needed
// (most are needed in the program-specific files)
// [TODO] After `bernese` release, remove this function completely?
const getPrefix = assetPath => '';


// =============================================================================
// Script Sources
// =============================================================================
const scriptSources = directory => [
    ...glob.sync(`${directory}/src/*.js`),
  ].map(source => ({
    assetPath       : source,
    outputDirectory : 'js/',
    outputExtension : '.js',
    prefix          : getPrefix(source)
  }));


// =============================================================================
// Polyfill Sources
// =============================================================================
const polyfillSources = directory => ([
  {
    assetPath: [
      '@babel/polyfill',
      'whatwg-fetch',
      `${directory}/resources/assets/js/helpers/NodeListPolyfill.js`
    ],
    name            : 'polyfills',
    outputDirectory : 'js/',
    outputExtension : '.js',
    prefix          : getPrefix(''),
  }
]);

// =============================================================================
// Entries Output
// =============================================================================
const entries = directory => [
  ...scriptSources(directory),
  // ...polyfillSources(directory),
].reduce((map, {
  assetPath, name, outputDirectory, outputExtension, prefix
}) => {
  const basename = name != null ? name : path.basename(assetPath, path.extname(assetPath));
  map[`${outputDirectory}${prefix}${basename}`] = assetPath;
  return map;
}, {});

module.exports = entries;
