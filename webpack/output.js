require('dotenv').config();

const output = (directory) => {
  return {
    path     : `${directory}/public`,
    filename : '[name].js',
  };
}

module.exports = output;
