import React from 'react';
import { render } from 'react-dom';

// Components
import Form from 'Components/Form';

// Variables
const rootElement = document.getElementById('root');

render(<Form />, rootElement);
