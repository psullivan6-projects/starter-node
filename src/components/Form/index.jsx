import React, { Component } from 'react';
import axios from 'axios';

// Styles
import {
  Figure,
  Form,
  Item,
  Image,
  UploadButton,
} from './styles';

console.log('Form', Form);

// [NOTE] This will be "instant" since we're using base64 URL
const handleImageLoad = (event) => {
  console.log('IMAGE HAS LOADED');
  event.target.classList.add('loaded');
};

class Test extends Component {
  state = {
    uploads: [],
  }

  constructor(props) {
    super(props);

    this.filesInput = React.createRef();
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData();
    const { files } = this.filesInput.current;

    // [NOTE] `photos` here must match `photos` from the express-based multer API processing
    Array.from(files).forEach(file => formData.append('photos', file));

    axios.post('/api/upload', formData)
      .then(response => response.data)
      .then((response) => {
        const data = response.map(item => ({
          src: `data:${item.mimetype};base64, ${item.base64}`,
          ...item,
        }));

        this.setState({
          uploads: data,
        });
      });
  }

  render() {
    const { uploads } = this.state;

    console.log('uploads', uploads);

    return (
      <section>
        <Form id="uploadForm" action="/api/upload" method="post" encType="multipart/form-data">
          <label htmlFor="fileUpload">
            File:
            <UploadButton ref={this.filesInput} id="fileUpload" type="file" name="photos" multiple onChange={this.handleSubmit} />
          </label>

          <button type="submit" onClick={this.handleSubmit}>Submit</button>
        </Form>
        {
          uploads.map(item => (
            <Figure>
              <Item {...item} />
              <Image src={item.src} alt="asdfas" onLoad={handleImageLoad} />
            </Figure>
          ))
        }
      </section>
    );
  }
}

export default Test;
