import styled from 'styled-components';

export const Form = styled.form`
  background-color: #ccc;
  padding: 3rem;
`;

export const Figure = styled.figure`
  display: flex;
`;

export const Image = styled.img`
  border: 4px solid black;

  &.loaded {
    border: 6px solid red;
  }
`;

export const UploadButton = styled.input`
  padding: 3rem;
  border: 2px solid black;
`;

export const Item = styled.div`
  flex-shrink: 0;
  width: 300px;
  height: ${({ aspectRatio }) => aspectRatio * 300}px;
  background-color: ${({ color }) => `rgb(${color.join(', ')})`};
`;

export default {};
