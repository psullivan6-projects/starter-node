require('dotenv').config();

const express = require('express');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const sharp = require('sharp');
const ColorThief = require('color-thief');

const router = express.Router();
const storage = multer.memoryStorage();
const upload = multer({ dest: 'uploads/', storage });


const routes = (publicDirectory) => {
  const dataDirectory = path.join(publicDirectory, 'data');

  // ===========================================================================
  // GET
  // ===========================================================================
  router.get('/uploads', (req, res) => {
    const file = fs.readFileSync(path.join(publicDirectory, 'data/uploads.json'));

    res.json(JSON.parse(file));
  });

  // ===========================================================================
  // POST
  // ===========================================================================
  router.post('/upload', upload.array('photos', 12), async (req, res) => {
    const files = await Promise.all(req.files.map(async (file) => {
      const newFile = {
        base64: file.buffer.toString('base64'),
        buffer: file.buffer,
        mimetype: file.mimetype,
        size: file.size,
        name: file.originalname,
      };

      const colorThief = new ColorThief();
      const color = colorThief.getColor(file.buffer);

      newFile.color = color;

      await sharp(file.buffer)
        .metadata()
        .then(({ width, height }) => {
          newFile.width = width;
          newFile.height = height;
          newFile.aspectRatio = Number(height) / Number(width);
        });

      return newFile;
    }));

    if (!fs.existsSync(dataDirectory)) {
      fs.mkdirSync(dataDirectory);
    }

    fs.writeFileSync(path.join(dataDirectory, 'uploads.json'), JSON.stringify(files, null, 2));

    res.json(files);
  });

  return router;
};

module.exports = routes;
