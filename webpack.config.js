const webpack = require('webpack');
const WebpackNotifierPlugin = require('webpack-notifier');

// Utilities
const aliases = require('./webpack/aliases')(__dirname);
const entries = require('./webpack/entries')(__dirname);
const environment = require('./scripts/utilities/getEnvironment')();
const externals = require('./webpack/externals');
const output = require('./webpack/output')(__dirname);

// Variables
const isDevMode = environment === 'local' || environment === 'development'; // For use in the build process
const isProductionMode = environment === 'staging' || environment === 'production'; // For use as a global variable AFTER the build

module.exports = {
  mode: isDevMode ? 'development' : 'production',
  entry: entries,
  output,
  devtool: isDevMode ? 'eval' : false,
  externals,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ],
  },
  plugins: [
    new WebpackNotifierPlugin({ alwaysNotify: true }),
    new webpack.DefinePlugin({
      BUILD_CONFIG: {
        isProductionMode,
        isDevMode,
      },
    }),
  ],
  resolve: {
    alias: aliases,
    extensions: ['.js', '.jsx'],
  },
  optimization: {
    runtimeChunk: isDevMode ? { name: 'js/runtime' } : false,
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'js/vendor',
          chunks: 'all',
        },
      },
    },
  },
};
